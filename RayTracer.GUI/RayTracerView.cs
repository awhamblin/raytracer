﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RayTracer.GUI
{
    public partial class RayTracerView : Form
    {
        public RayTracerView()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.BorderStyle = BorderStyle.FixedSingle;
            pictureBox1.Anchor = (AnchorStyles.Top | AnchorStyles.Left);
            var height = 400;
            var flag = new Bitmap(height, height);
            for (int x = 0; x < flag.Height; ++x)
                for (int y = 0; y < flag.Width; ++y)
                {
                    var r = RayTracer.Color.ScaleToByte(x, flag.Height) ;
                    var g = RayTracer.Color.ScaleToByte(y, flag.Width);
                    var c = System.Drawing.Color.FromArgb(r, g, 100);
                    flag.SetPixel(x, y, c);
                }
            pictureBox1.Image = flag;
            Resize += OnMainWindowResize;
        }

        private void OnMainWindowResize(object o, EventArgs e)
            {
                var size = new Size(Size.Width - 40, Size.Height - 60);
                pictureBox1.Size = size;
                
            }
        }
    }

