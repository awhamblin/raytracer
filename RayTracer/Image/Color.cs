﻿namespace RayTracer
{
    public class Color
    {
        public float Red { get; set; }
        public float Green { get; set; }
        public float Blur { get; set; }

        public static int ScaleToByte(int x, int m)
        {
            return (int) (((float) x/(float)m) * 255.0);
        }
    }
}