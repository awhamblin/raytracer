using System.Collections;
using System.Security.Cryptography.X509Certificates;

namespace RayTracer
{
    public class Image
    {
        private Color[][] _data;
        private int _width, _height;

        public Image(int width, int height)
        {
            _width = width;
            _height = height;
            InitializeBackingData(width);
        }

        private void InitializeBackingData(int width)
        {
            _data = new Color[width][];
            for (var x = 0; x < _width; x++)
            {
                _data[x] = new Color[_height];
            }
        }

        public Color this[int x, int y]
        {
            get { return _data[x][y]; }
        }
    }
}