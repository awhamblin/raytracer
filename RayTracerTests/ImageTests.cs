﻿using System;
using NUnit.Framework;
using Shouldly;

namespace RayTracerTests
{

    [TestFixture]
    public class ImageTests 
    {
        [Test]
        public void ImagesHavePixels()
        {
            var test = new RayTracer.Image(1, 1);
            test[0,0].ShouldBeNull();
        }
    }
}